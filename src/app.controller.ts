import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventBus, QueryBus } from '@nestjs/cqrs';
import * as uuid from 'uuid';

import { OrderEvent } from './order/order.events';  

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
              private readonly eventBus: EventBus,
              private queryBus: QueryBus) {}

  @Get( )
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("/path")
  async bid(): Promise<object> {

    const orderTransactionGUID = uuid.v4();

    this.eventBus.publish(
      new OrderEvent(
        orderTransactionGUID, 'Daniel Trimson', 'Samsung LED TV', 50000),
    );

    return {
      status: 'PENDING',
    };
  }
}
